<?php

use Faker\Generator as Faker;

$factory->define(App\Story::class, function (Faker $faker) {
    return [
        'title' => $faker->text(50),
        'text' => $faker->text(200),
        'descendants' => $faker->numberBetween(0, 200),
        'score' => $faker->numberBetween(0, 200),
        'url' => $faker->url,
    ];
});
